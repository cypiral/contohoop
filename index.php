<?php

//require("animal.php");
//require("frog.php");
require("ape.php");
 
 

$sheep = new animal("shaun");

echo "Name :   $sheep->name <br>"; // "shaun"
echo "Legs :   $sheep->legs <br>"; // 4
echo "cold_blooded :   $sheep->cold_blooded <br>"; // "no"
echo "<br>";
$sheep2 = new frog("Buduk");

echo "Name :   $sheep2->name <br>"; // "shaun"
echo "Legs :   $sheep2->legs <br>"; // 4
echo "cold_blooded :   $sheep2->cold_blooded <br>"; // "no"
$sheep2->jump() ; 
echo "<br>";

$sheep3 = new ape("Kera Sakti");
echo "<br>";
echo "Name :   $sheep3->name <br>"; // "shaun"
echo "Legs :   $sheep3->legs <br>"; // 4
echo "cold_blooded :   $sheep3->cold_blooded <br>"; // "no"
$sheep3->yel() ; // "no"
